package com.example.project4_danielaquaries

import android.app.Application
import android.content.Context
import com.crocodic.core.helper.SessionHelper

class MyApplication : Application() {

    companion object {
        @JvmField
        var context: Context? = null

        @JvmStatic
        fun getMyApplicationContext(): Context? {
            return context

        }

        fun getSession(): SessionHelper? {
            val context = MyApplication.getMyApplicationContext()
            val session = context?.let { SessionHelper(it) }
            session?.setName("prefs")
            return session
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}