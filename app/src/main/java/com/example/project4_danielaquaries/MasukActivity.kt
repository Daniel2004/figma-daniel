package com.example.project4_danielaquaries

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.project4_danielaquaries.model.TokenModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_masuk.*
import okhttp3.ResponseBody
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MasukActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_masuk)
        textUsername.text = intent.getStringExtra("username")
        textEmail.text = intent.getStringExtra("email")
        textPassword.text = intent.getStringExtra("password")

        buttonBack.onClick { onBackPressed() }

        getToken()
        loginEmail()


    }

    private fun getToken() {
        ApiService.instance.getToken().enqueue(object : Callback<TokenModel> {
            override fun onFailure(call: Call<TokenModel>, t: Throwable) {
                toast("Error")
            }

            override fun onResponse(call: Call<TokenModel>, response: Response<TokenModel>) {

                val tokenModel = response.body()!!
                val getToken = tokenModel.data?.token.toString()
                MyApplication.getSession()?.save("access_token", getToken)

                Log.d("token", tokenModel.toString())


                textToken.text = "Token : ${tokenModel.data?.token}"

                MyApplication.getSession()?.save("access_token", tokenModel.toString())

            }

        })
    }



    private fun loginEmail() {
        ApiService.instance.loginByEmail("123456", "developer@crocodic.com", "123456")
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    toast("Gagal")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val json = response.body()?.string()
                    val jsonObject = JSONObject(json)
                    if (jsonObject.getInt("status") == 0) {
                        MyApplication.getSession()?.remove("access_token")
                        getToken()
                    }
                }
            })
    }
}