package com.example.project4_danielaquaries.api

import com.example.project4_danielaquaries.model.TokenModel
import com.example.project4_danielaquaries.model.login_request.LoginByEmail
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface GetAPI {

    @GET("auth/token ")
    fun getToken(): Call<TokenModel>

    @FormUrlEncoded
    @POST("auth/login-email")
    fun loginByEmail(
        @Field("regid") regId: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("auth/register")
    fun postRegister(
        @Field("regid") regIdRegister: String?,
        @Field("nama") namaRegister: String?,
        @Field("email") emailRegister: String?,
        @Field("no_hp") no_hpRegister: String?,
        @Field("password") passwordRegister: String?,
        @Field("type") typeRegister: String?,
        @Field("sosial_id") sosial_idRegister: String?
    ): Call<ResponseBody>

}