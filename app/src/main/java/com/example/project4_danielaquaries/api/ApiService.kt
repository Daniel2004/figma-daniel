import android.util.Base64
import com.crocodic.core.helper.SessionHelper
import com.example.project4_danielaquaries.MyApplication
import com.example.project4_danielaquaries.MyApplication.Companion.getSession
import com.example.project4_danielaquaries.api.GetAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*

object ApiService {

    private const val BASE_URL = "https://neptune2.crocodic.net/gain/public/api/v1/"
    private const val APP_KEY = "S5NjdQWDhHTCFhp9"
    private const val APP_ID = "nXJ1qhkIHlyuLxeJ"

    val instance: GetAPI by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClient())
            .build()


        retrofit.create(GetAPI::class.java)
    }

    private fun getOkHttpClient(): OkHttpClient {


        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.addInterceptor { chain ->
            val original = chain.request()
            val session = getSession()
            val accessToken = session?.get("access_token", "")
            val requestBuilder = original.newBuilder()

            if (!accessToken.isNullOrEmpty()) {
                requestBuilder.header("Token", "Barrer $accessToken")
            } else {
                requestBuilder.header("Authorization", getBasicAuth())
            }
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)

        return okHttpClientBuilder.build()

    }

    private fun getBasicAuth(): String {
        val pass = getCurrentDate() + "|" + APP_KEY
        val credentials = "$APP_ID:" + Base64.encodeToString(
            pass.toByteArray(),
            Base64.NO_WRAP
        )
        return "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
    }

    private fun getCurrentDate(): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return formatter.format(Date())
    }



}