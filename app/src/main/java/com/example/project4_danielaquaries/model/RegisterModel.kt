package com.example.project4_danielaquaries.model

import com.google.gson.annotations.SerializedName

data class RegisterModel (

    @SerializedName("status")
    val statusRegister: Int,

    @SerializedName("type")
    val typeRegister: String,

    @SerializedName("message")
    val messageRegister: String,

    @SerializedName("data")
    val dataRegister: List<GetData>
)

data class GetData(

    @SerializedName("id")
    val idRegister: Int,

    @SerializedName("foto")
    val fotoRegister: String,

    @SerializedName("nama")
    val emailRegister: String,

    @SerializedName("telfon")
    val telfonRegister: String,

    @SerializedName("type")
    val typeDataRegister: String
)