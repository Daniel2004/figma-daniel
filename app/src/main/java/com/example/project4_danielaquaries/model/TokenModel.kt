package com.example.project4_danielaquaries.model

import com.google.gson.annotations.SerializedName

data class TokenModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Data(

	@field:SerializedName("expired")
	val expired: Int? = null,

	@field:SerializedName("token")
	val token: String? = null
)
