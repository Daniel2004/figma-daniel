package com.example.project4_danielaquaries

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonLogin.onClick {
            val intent = Intent(this@MainActivity, MasukActivity::class.java)
            intent.putExtra("username", editTextUsername.text.toString())
            intent.putExtra("email", editTextEmail.text.toString())
            intent.putExtra("password", editTextPassword.text.toString())
            startActivity(intent)
        }

        buttonRegister.onClick {
            val username = editTextUsername.text.toString().trim()
            MyApplication.getSession()?.save("Username", username)

            val loadUsername = MyApplication.getSession()?.get("Username", "test")
            toast("Username : $loadUsername")

        }


    }
}