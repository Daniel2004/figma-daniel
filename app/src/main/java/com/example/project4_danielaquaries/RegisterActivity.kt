package com.example.project4_danielaquaries

import ApiService
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    var strName = ""
    var strEmail = ""
    var strPassword = ""
    var strNoTelpon = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        registerApi()

    }

    private fun registerApi() {

        buttonNextRegister.setOnClickListener {
            strName = editTextUsernameRegister.text.toString()
            strEmail = editTextEmailRegister.text.toString()
            strPassword = editTextPasswordRegister.text.toString()
            strNoTelpon = editTextNomorTelponRegister.text.toString()

//            strEmail = "daniel@gmail.com"
//            strNoTelpon = "123456"

//            if (strEmail.isEmpty() && strNoTelpon.isNotEmpty()) {
//                toast("Hanya Nomer Telepon yang Terisi")
//            }

            ApiService.instance.postRegister("" ,"$strName","$strEmail","$strNoTelpon","$strPassword","","")
                .enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast("Gagal")
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        val register = response.body()?.string()
                        val registerObject = JSONObject(register)

                    }
                })

            if (editTextUsernameRegister.text.toString().trim().isEmpty()) {
                editTextUsernameRegister.error = "Harap Diisi"
            } else if (editTextEmailRegister.text.toString().trim().isEmpty()) {
                editTextEmailRegister.error = "Harap Diisi"
            } else if (editTextPasswordRegister.text.toString().trim().isEmpty()) {
                editTextPasswordRegister.error = "Harap Diisi"
            } else if (editTextNomorTelponRegister.text.toString().trim().isEmpty()) {
                editTextNomorTelponRegister.error = "Harap Diisi"
            }
        }

    }

}